FROM node:lts-alpine

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
COPY ./ ./

RUN npm install

EXPOSE 4200
EXPOSE 8080

CMD ["npm", "start"]